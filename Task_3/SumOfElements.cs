﻿namespace one;

public class SumOfElements
{
    public void ReadFile()
    {
        Console.WriteLine("Enter path to file");
        string pathToFile = Console.ReadLine();

        if (String.IsNullOrEmpty(pathToFile))
        {
            throw new ArgumentNullException();
        }
        
        FileProcessing(pathToFile);

    }
    private void FileProcessing(string pathToFile)
    {
        if(String.IsNullOrEmpty(pathToFile))
        {
            throw new ArgumentNullException();
        }
        string[] stringsInFile = File.ReadAllLines(pathToFile);
        int sum = 0;
        int sumMax = 0;
        int targetIndex = 0;

        for (int i = 0; i < stringsInFile.Length; i++)
        {
            for (int j = 0; j < stringsInFile[i].Split(',').Length; j++)
            {
                bool result = int.TryParse(stringsInFile[i].Split(',')[j], out _);
                if (result == true)
                {
                    sum += int.Parse(stringsInFile[i].Split(',')[j]);
                }
                else if(String.IsNullOrEmpty(stringsInFile[i].Split(',')[j]))
                {
                    Console.WriteLine($"Line {i} is empty");
                }
            }
            if (sum > sumMax)
            {
                sumMax = sum;
                targetIndex = i;

            }
            sum = 0;
        }
        Console.WriteLine($"Line whith the largest sum {targetIndex} = {sumMax}");
    }
}
